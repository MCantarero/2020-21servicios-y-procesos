#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){

    printf("El nombre de este programa es '%s' \n",argv[0]);

    printf("Este programa se ha invocado con %d argumentos \n",argc -1);

    if(argc > 1){
        int i;
        printf("los argumentos son: \n ");
        for(i=1; i<argc; i++)
            printf("%s \n",argv[i]);
    }


    return EXIT_SUCCESS;
}
