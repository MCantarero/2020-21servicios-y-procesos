#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void forkexample()
{
	int x = 1;

	if (fork() == 0)//al ser el proceso hijo, el printf mostraria 
		printf("Child has x = %d\n", ++x);//"Child has x = 2"
	else//al ser el proceso padre, el printf mostraria 
		printf("Parent has x = %d\n", --x);//"Parent has = 0"
}
int main()
{
	forkexample();
	return 0;
}

