#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
void forkexample()
{
     
    if (fork() == 0)//fork devuelve un 0 en el proceso hijo 
        printf("Hello from Child!\n");//por lo tanto esta linea se ejecuta en el 
    //proceso hijo

    
    else //Esta linea se ejecuta en el proceso padre, porque fork() devuelve el PID 
        printf("Hello from Parent!\n");//del proceso hijo, que es un numero distinto de 0
}
int main()
{
    forkexample();
    return 0;
}
