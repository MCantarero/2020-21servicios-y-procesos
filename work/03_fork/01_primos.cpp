#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <iostream>
#include <signal.h>
#define HUECO_PILA 5


sig_atomic_t senial_siguser1=0;

bool seguir;

struct TPila{

    int *array;
    unsigned cima;
    unsigned tamanio;//el tamaño de array se calcula asi -->> tamanio * HUECO_PILA
    unsigned p_ultimo_primo;


};

void inicializacion(struct TPila *P){
    P->array =NULL; //(int *) malloc(HUECO_PILA *sizeof(int) );
    P->cima = 0;
    P->tamanio =0;

}

void crear_hueco(struct TPila *P){

    P->tamanio += HUECO_PILA;
    P->array = (int *) realloc( P->array, P->tamanio *sizeof(int) );

}
void empujar_dato (struct TPila *P,unsigned int numero_primo){

    if( P->cima >= P->tamanio )
        crear_hueco(P);


    P->array[P->cima]=numero_primo;
    P->p_ultimo_primo=P->cima;
    P->cima++;


}


void liberar_pila( struct TPila *P){
    free(P->array);

}

void mostrar_numeros(struct TPila *P){

    for(unsigned int i=0; i<P->cima;i++)
        printf("%u \n",P->array[i]);

    printf("primos calculados: %u",P->p_ultimo_primo);
}


void cambio_estado( ){
    seguir=false;
}

void crear_fichero(){

}
void rellenar_fichero(){}


void mi_handler(int signal){
    
    ++senial_siguser1;
    cambio_estado();
}




void calcular_primo(struct TPila *P){

    unsigned int primo_potencial;
    seguir=true;

    while(seguir){
        primo_potencial = P->array[ P->p_ultimo_primo ] + 2;

        if(!primo_potencial % 5 == 0){
        for( unsigned int i=0; i<P->cima; i++ )
            if( primo_potencial % P->array[i] == 0){
                primo_potencial += 2;
                i = 0;
            }
        }else{
            primo_potencial+=2;
        }




        empujar_dato(P,primo_potencial);


        //condicion de parada temporal para mirar si funciona

       /* if(P->p_ultimo_primo == 200000)
           seguir=false;
       */
    }
}




int main (int argc, char *argv[]){

    struct TPila pilaprimos;

    signal(SIGINT,&mi_handler);
    
    inicializacion(&pilaprimos);

    empujar_dato(&pilaprimos,2);

    empujar_dato(&pilaprimos,3);

    empujar_dato(&pilaprimos,5);

    calcular_primo(&pilaprimos);
    
       // cambio_estado(&pilaprimos);

    mostrar_numeros(&pilaprimos);

    liberar_pila(&pilaprimos); 
    return EXIT_SUCCESS;

}







