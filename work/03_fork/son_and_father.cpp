#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
    pid_t child_pid;

    printf ("El ID del programa principal es %d\n", (int) getpid ());

    child_pid = fork ();

    if (child_pid != 0) {
        printf ("Este es el ID del proceso padre: %d\n", (int) getpid ());
        printf ("Este es el ID del proceso hijo %d \n", (int) child_pid);
    }
    else
        printf ("El fork funciono con exito, ID de proceso hijo es: %d\n", (int) getpid ());
    return 0;
}
